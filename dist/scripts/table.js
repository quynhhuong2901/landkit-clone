var dataSet = [
    ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421', '2011/04/25', '$320,800'],
    ['Garrett Winters', 'Accountant', 'Tokyo', '8422', '2011/07/25', '$170,750'],
    ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562', '2009/01/12', '$86,000'],
    ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224', '2012/03/29', '$433,060'],
    ['Airi Satou', 'Accountant', 'Tokyo', '5407', '2008/11/28', '$162,700'],
    ['Brielle Williamson', 'Integration Specialist', 'New York', '4804', '2012/12/02', '$372,000'],
    ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608', '2012/08/06', '$137,500'],
    ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200', '2010/10/14', '$327,900'],
    ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360', '2009/09/15', '$205,500'],
    ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667', '2008/12/13', '$103,600'],
    ['Jena Gaines', 'Office Manager', 'London', '3814', '2008/12/19', '$90,560'],
    ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497', '2013/03/03', '$342,000'],
    ['Charde Marshall', 'Regional Director', 'San Francisco', '6741', '2008/10/16', '$470,600'],
    ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597', '2012/12/18', '$313,500'],
    ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965', '2010/03/17', '$385,750'],
    ['Michael Silva', 'Marketing Designer', 'London', '1581', '2012/11/27', '$198,500'],
    ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059', '2010/06/09', '$725,000'],
    ['Gloria Little', 'Systems Administrator', 'New York', '1721', '2009/04/10', '$237,500'],
    ['Bradley Greer', 'Software Engineer', 'London', '2558', '2012/10/13', '$132,000'],
    ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290', '2012/09/26', '$217,500'],
    ['Jenette Caldwell', 'Development Lead', 'New York', '1937', '2011/09/03', '$345,000'],
    ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154', '2009/06/25', '$675,000'],
    ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330', '2011/12/12', '$106,450'],
    ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023', '2010/09/20', '$85,600'],
    ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797', '2009/10/09', '$1,200,000'],
    ['Gavin Joyce', 'Developer', 'Edinburgh', '8822', '2010/12/22', '$92,575'],
    ['Jennifer Chang', 'Regional Director', 'Singapore', '9239', '2010/11/14', '$357,650'],
    ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314', '2011/06/07', '$206,850'],
    ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947', '2010/03/11', '$850,000'],
    ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899', '2011/08/14', '$163,000'],
    ['Michelle House', 'Integration Specialist', 'Sydney', '2769', '2011/06/02', '$95,400'],
    ['Suki Burks', 'Developer', 'London', '6832', '2009/10/22', '$114,500'],
    ['Prescott Bartlett', 'Technical Author', 'London', '3606', '2011/05/07', '$145,000'],
    ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860', '2008/10/26', '$235,500'],
    ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240', '2011/03/09', '$324,050'],
    ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384', '2009/12/09', '$85,675'],
];

var table = document.getElementById("myTable")
var tableBody = document.querySelector(".tableBody")
var inputSearch = document.getElementById('dataSearch')
var select = ''
var rows_per_page = 10
const pagination_element = document.getElementById('pagination');
var current_page = 1
var start, end = 0
var dataNew = dataSet
var current_page_now = 1
var page_count = 1

// Render table
function renderTable(data=[], page=1){
    let removeTrs = document.querySelectorAll("tbody tr")
    removeTrs.forEach((tr) => {
        tr.remove()
    })

    page--;
    start = rows_per_page*page;
    end = start + parseInt(rows_per_page);
    let paginatedItems = data.slice(start,end)

    paginatedItems.forEach((val) => {
        let currentNumberRows = document.querySelectorAll("#myTable tr").length - 1;
        if (currentNumberRows<rows_per_page){
            let tr = document.createElement("tr")
            tr.innerHTML = `
            <td>${val[0]}</td>
            <td>${val[1]}</td>
            <td>${val[2]}</td>
            <td>${val[3]}</td>
            <td>${val[4]}</td>
            <td>${val[5]}</td>
            `
            tableBody.append(tr)
        } else {
            return;
        }
    })
    // Sau khi render data -> set pagination
    setPagination(data, pagination_element, rows_per_page);

    let lengthData = data.length
    if(end>lengthData){
        end=lengthData
    }
    if(lengthData==0){
        document.getElementById('example_info').innerHTML = `Showing ${start} to ${end} of ${lengthData} entries`
    } else {
        document.getElementById('example_info').innerHTML = `Showing ${start+1} to ${end} of ${lengthData} entries`
    }

}
// Rows per page
function rowsPerPage(data=[]){
    select = document.getElementById('number_rows')
    rows_per_page = select.options[select.selectedIndex].value

    renderTable(data)
    inputSearch.value = ''
}

// Pagination
function setPagination(data, wrapper, rows_per_page){
    // var buttons = document.querySelectorAll('#pagination button')
    // buttons.forEach((button) => {
    //     button.remove()
    // })

    wrapper.innerHTML = "";
    page_count = Math.ceil(data.length/rows_per_page)
    
    for(let i = 1; i < page_count + 1; i++){
        let btn = PaginationButton(i, data);
        wrapper.appendChild(btn)
    }


}

function PaginationButton(page, data=[]){
    let button = document.createElement('button');
    button.innerText = page;
    button.setAttribute('id', page)

    if(current_page == page) button.classList.add('active');
    button.addEventListener('click', function() {
        current_page_now = page;
        current_page=page;
        // Render data table
        renderTable(data, current_page);
        //Active button Pagination
		button.classList.add('active');
    })
    return button;
}

// Search data
function searchDataTable(){
    let dataInput = inputSearch.value.toLowerCase()
    if(dataInput!=''){
        dataNew = dataSet.filter((val) => val[0].toLowerCase().includes(dataInput) || val[1].toLowerCase().includes(dataInput) || val[2].toLowerCase().includes(dataInput)
            || val[3].toLowerCase().includes(dataInput) || val[4].toLowerCase().includes(dataInput) || val[5].toLowerCase().includes(dataInput))
    } else {
        dataNew = dataSet
    }
    
    renderTable(dataNew)
}

// Sort data
let sortAsc=true;
function sort(e){
    var target = e.target
    var th = target.closest('th')
    var valueSort = th.dataset.sort
    var data = []
    if (dataNew == '') {
        data = dataSet
    } else {
        data = dataNew
    }
    if(valueSort=='name') {
        if(sortAsc){
            data.sort((a,b) => (a[0] > b[0]) ? 1 : ((b[0] > a[0]) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (a[0] < b[0]) ? 1 : ((b[0] < a[0]) ? -1 : 0))
            sortAsc=true
        }
    }
    if(valueSort=='position') {
        if(sortAsc){
            data.sort((a,b) => (a[1] > b[1]) ? 1 : ((b[1] > a[1]) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (a[1] < b[1]) ? 1 : ((b[1] < a[1]) ? -1 : 0))
            sortAsc=true
        }
    }
    if(valueSort=='office') {
        if(sortAsc){
            data.sort((a,b) => (a[2] > b[2]) ? 1 : ((b[2] > a[2]) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (a[2] < b[2]) ? 1 : ((b[2] < a[2]) ? -1 : 0))
            sortAsc=true
        }
    }
    if(valueSort=='extn') {
        if(sortAsc){
            data.sort((a,b) => (a[3] > b[3]) ? 1 : ((b[3] > a[3]) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (a[3] < b[3]) ? 1 : ((b[3] < a[3]) ? -1 : 0))
            sortAsc=true
        }
    }
    if(valueSort=='startDate') {
        if(sortAsc){
            data.sort((a,b) => (a[4] > b[4]) ? 1 : ((b[4] > a[4]) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (a[4] < b[4]) ? 1 : ((b[4] < a[4]) ? -1 : 0))
            sortAsc=true
        }
    }
    if(valueSort=='salary') {
        if(sortAsc){
            data.sort((a,b) => (parseInt(a[5].replace(/[^0-9\.]+/g,"")) > parseInt(b[5].replace(/[^0-9\.]+/g,""))) ? 1 : ((parseInt(b[5].replace(/[^0-9\.]+/g,"")) > parseInt(a[5].replace(/[^0-9\.]+/g,""))) ? -1 : 0))
            sortAsc=false
        } else {
            data.sort((a,b) => (parseInt(a[5].replace(/[^0-9\.]+/g,"")) < parseInt(b[5].replace(/[^0-9\.]+/g,""))) ? 1 : ((parseInt(b[5].replace(/[^0-9\.]+/g,"")) < parseInt(a[5].replace(/[^0-9\.]+/g,""))) ? -1 : 0))
            sortAsc=true
        }
    }
    renderTable(data)
}

renderTable(dataSet)
// setPagination(dataSet, pagination_element, rows_per_page);

// listen for sort clicks
document.querySelectorAll('#myTable thead tr th').forEach(t => {
    t.addEventListener('click', sort, false);
});

var pre = document.getElementById('pre')
var next = document.getElementById('next')

pre.addEventListener('click', function(){
    if(current_page_now>1) current_page_now--;
    // Render data table
    renderTable(dataNew, current_page_now);
    var buttons = document.querySelectorAll('#pagination button')
    buttons.forEach((button) => {
        button.classList.remove('active')
    })
    var current_btn = document.getElementById(current_page_now)
    current_btn.classList.add('active')
})

next.addEventListener('click', function() {
    if(current_page_now<page_count) current_page_now++;
    // Render data table
    renderTable(dataNew, current_page_now);
    var buttons = document.querySelectorAll('#pagination button')
    buttons.forEach((button) => {
        button.classList.remove('active')
    })
    var current_btn = document.getElementById(current_page_now)
    current_btn.classList.add('active') 
})



