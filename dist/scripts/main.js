const typedTextSpan = document.querySelector(".typed-text")
const textArray = ["developers.", "founders.", "designers."]
const typingDelay = 50;
const erasingDelay = 50;
const newTextDelay = 100;
let textArrayIndex = 0;
let charIndex = 0;

function type() {
    if(charIndex<textArray[textArrayIndex].length){
        typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex)
        charIndex++;
        setTimeout(type, typingDelay);
    } else {
        setTimeout(erase, newTextDelay+1100);
    }
}

function erase(){
    if(charIndex>0){
        typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
        charIndex--;
        setTimeout(erase, erasingDelay);
    } else {
        textArrayIndex++;
        if(textArrayIndex>=textArray.length) textArrayIndex=0;
        setTimeout(type, typingDelay);
    }

}

document.addEventListener("DOMContentLoaded", function(){
    setTimeout(type, newTextDelay+250);
});

// Responsive show menu navigation
var btnMenu = document.getElementById("btn-toggle");
btnMenu.addEventListener("click", function(){
    var responsiveMenu = document.getElementsByClassName("nav-with-button")[0]
    var toggleCloseBtn = document.getElementsByClassName("toggle-close-btn")[0]
    var html = document.querySelector('html')
    responsiveMenu.classList.toggle("show-navbar");
    toggleCloseBtn.classList.toggle("show");
    html.style.overflow="hidden"


})
 
var btnCloseToggle = document.getElementsByClassName("toggle-close-btn")[0]
btnCloseToggle.addEventListener("click", function(){
    var responsiveMenu = document.getElementsByClassName("nav-with-button")[0]
    var toggleCloseBtn = document.getElementsByClassName("toggle-close-btn")[0]
    var html = document.querySelector('html')
    
    responsiveMenu.classList.remove("show-navbar");
    toggleCloseBtn.classList.remove("show");
    html.style.overflowY="auto"
    html.style.overflowX="hidden"
})

$(document).on("click",".fa-angle-down",function(e){
    var id = $(this).data("id")
    $(`.i-${id}`).toggleClass("icon-rotate")
    $(`.subnav-item-${id}`).toggleClass("show")  
})

// Slider
var slideIndex = 0;
$(document).on("click", ".btn-slider-left", function(){
    let imageUrl = ""
    if (slideIndex==0){
        imageUrl = "/dist/img/photo-26.jpg";
        $(".blockquote-0").hide();
        $(".blockquote-1").show();
        slideIndex=1;
    } else {
        imageUrl = "/dist/img/photo-1.jpg";
        $(".blockquote-1").hide();
        $(".blockquote-0").show();
        slideIndex=0;
    }
    $('.card-img').css('background-image', 'url(' + imageUrl + ')');
})
$(document).on("click", ".btn-slider-right", function(e){
    let imageUrl = ""
    if (slideIndex==0){
        imageUrl = "/dist/img/photo-26.jpg";
        $(".blockquote-0").hide();
        $(".blockquote-1").show();
        slideIndex=1;
    } else {
        imageUrl = "/dist/img/photo-1.jpg";
        $(".blockquote-1").hide();
        $(".blockquote-0").show();
        slideIndex=0;
    }
    $('.card-img').css('background-image', 'url(' + imageUrl + ')');

})


// Animation Scroll
function reveal() {
    var reveals = document.querySelectorAll(".reveal");
    for (var i = 0; i < reveals.length; i++) {
      var windowHeight = window.innerHeight;
      var elementTop = reveals[i].getBoundingClientRect().top;
      var elementVisible = 150;
      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add("active");
      } else {
        reveals[i].classList.remove("active");
      }
    }
}
function reveal2() {
    var reveals = document.querySelectorAll(".reveal-2");
    for (var i = 0; i < reveals.length; i++) {
      var windowHeight = window.innerHeight;
      var elementTop = reveals[i].getBoundingClientRect().top;
      var elementVisible = 150;
      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add("active2");
      } else {
        reveals[i].classList.remove("active2");
      }
    }
}
function reveal3() {
    var reveals = document.querySelectorAll(".reveal-3");
    for (var i = 0; i < reveals.length; i++) {
      var windowHeight = window.innerHeight;
      var elementTop = reveals[i].getBoundingClientRect().top;
      var elementVisible = 150;
      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add("active3");
      } else {
        reveals[i].classList.remove("active3");
      }
    }
}
window.addEventListener("scroll", reveal);
window.addEventListener("scroll", reveal2);
window.addEventListener("scroll", reveal3);

// To check the scroll position on page load
reveal();
reveal2()


// Check validate form
function setError(ele, message) {
    let parentEle = ele.parentNode;
    parentEle.classList.add('error');
    parentEle.querySelector('small').innerText = message;
}
function setSuccess(ele){
    let parentEle = ele.parentNode;
    parentEle.classList.remove('error');
}
function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
    );
}
const nameInput = document.getElementById('formName')
const emailInput = document.getElementById('formEmail')
const passwordInput = document.getElementById('formPassword')

function validateForm() {
    let nameValue = nameInput.value;
    let emailValue = emailInput.value;
    let passwordValue = passwordInput.value;
    let isChecked = true;
    if(!nameValue){
        setError(nameInput, "Please provide your name!")
        isChecked = false
    }  else {
        setSuccess(nameInput)
    }
    if(!emailValue){
        setError(emailInput, "Please provide your email!")
        isChecked = false

    } else if(!isEmail(emailValue))  {
        setError(emailInput, "Please provide correct email!")
        isChecked = false
    }
    else {
        setSuccess(emailInput)
    }
    if(!passwordValue){
        setError(passwordInput, "Please provide your password!")
        isChecked = false
    } else if(passwordValue.length < 8){
        setError(passwordInput, "The length of the password must >= 8")
        isChecked = false
    } 
    else {
        setSuccess(passwordInput)
    }
    return isChecked
}

btnDownload = document.getElementById("btnDownload")
btnDownload.addEventListener("click", function(e) {
    e.preventDefault()
    if(validateForm()!= false){
        mylist = validateForm()
        console.log("Name: " + nameInput.value)
        console.log("Email: " + emailInput.value)
        console.log("Password: " + passwordInput.value)
    }
})